﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace BankingApi.Service
{
    static class HelperClass
    {
        public static TransactionScope GetTransactionScope() =>
            new TransactionScope(
                scopeOption: TransactionScopeOption.Required,
                transactionOptions: new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted },
                asyncFlowOption: TransactionScopeAsyncFlowOption.Enabled
            );
    }
}
