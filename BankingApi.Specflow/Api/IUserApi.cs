﻿using Refit;
using System.Threading.Tasks;

namespace BankingApi.Specflow.Api
{
    interface IUserApi
    {
        [Get("/api/User/CheckIfUserExists?email=test@user.com")]
        Task<ApiResponse<bool>> CheckIfUserExists();

        [Get("/api/User")]
        Task<ApiResponse<UserModel>> GetUser();
    }
}
