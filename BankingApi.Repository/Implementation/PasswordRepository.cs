﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public class PasswordRepository : IPasswordRepository
    {
        private readonly BankingContext context;

        public PasswordRepository(BankingContext context)
        {
            this.context = context;
        }

        public async Task SavePasswordAsync(Password password)
        {
            await context.AddAsync(password);
            var result = await context.SaveChangesAsync();
        }

        public async Task<Password> GetCurrentForUserAsync(long userId)
        {
            return await context.Passwords.FirstOrDefaultAsync(o => o.UserId == userId && o.IsCurrent);
        }
    }
}
