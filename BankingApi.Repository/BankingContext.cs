﻿using System;
using System.Collections.Generic;
using System.Text;
using BankingApi.Repository;
using Microsoft.EntityFrameworkCore;

namespace BankingApi.Repository
{
    public class BankingContext : DbContext
    {
        public BankingContext(DbContextOptions<BankingContext> options) : base(options) { }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Password> Passwords { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
    }
}
