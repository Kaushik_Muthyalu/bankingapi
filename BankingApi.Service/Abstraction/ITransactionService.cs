﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankingApi.Service
{
    public interface ITransactionService
    {
        Task<List<TransactionModel>> GetAllUserTransactionsAsync(long userId);
        Task<TransactionModel> GetrTransactionAsync(long userId, long transactionId);

        Task<long> DepositAsync(long userId, TransactionModel transactionModel);
        Task<long> WithdrawAsync(long userId, TransactionModel transactionModel);
    }
}