﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly BankingContext context;

        public UserRepository(BankingContext context)
        {
            this.context = context;
        }

        public async Task AddUserAsync(User user)
        {
            await context.AddAsync(user);

            await context.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(User user)
        {
            context.Update(user);

            await context.SaveChangesAsync();
        }

        public async Task<User> GetUserAsync(long id)
        {
            return await context.Users.FirstOrDefaultAsync(o => o.Id == id);
        }

        public async Task<bool> CheckIfEmailExistsAsync(string email)
        {
            return await context.Users.AnyAsync(o => o.Email == email);
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await context.Users.FirstOrDefaultAsync(o => o.Email == email);
        }
    }
}
