﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BankingApi.Specflow
{
    public class TransactionModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Amount required")]
        [Range(0.0, double.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public decimal Amount { get; set; }

        public string TransactionType { get; set; }

        public string TransactionTime { get; set; }
    }
}
