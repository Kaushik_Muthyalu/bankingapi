﻿using BankingApi.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankingApi.App
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService userService;

        private readonly long _userId;

        public UserController(ILogger<UserController> logger, IAuthorizeService authorizeService, IUserService userService)
        {
            _logger = logger;
            this.userService = userService;

            _userId = Convert.ToInt64(authorizeService.Sub);
        }

        [AllowAnonymous]
        [HttpGet("CheckIfUserExists")]
        public async Task<IActionResult> CheckIfUserExists([FromQuery]string email)
        {
            try
            {
                var userExists = await userService.CheckUserExistsAsync(email);

                return Ok(userExists);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            try
            {
                var user = await userService.GetUser(_userId);

                return Ok(user);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }
    }
}
