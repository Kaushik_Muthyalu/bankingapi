﻿using BankingApi.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankingApi.App
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IUserService userService;
        private readonly IAuthenticateService authenticateService;

        public AuthController(ILogger<AuthController> logger, IUserService userService, IAuthenticateService authenticateService)
        {
            _logger = logger;
            this.userService = userService;
            this.authenticateService = authenticateService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] SignUpModel user)
        {
            try
            {
                var userExists = await userService.CheckUserExistsAsync(user.Email);

                if (userExists)
                    return Conflict(new AuthResponseModel { Success = false, Error = "User already exists" });

                long userId = await userService.TrySignUpAsync(user);


                var token = authenticateService.GenerateToken(userId.ToString());

                return Ok(token);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginModel user)
        {
            try
            {
                var userExists = await userService.CheckUserExistsAsync(user.Email);

                if (!userExists)
                    return NotFound(new AuthResponseModel { Success = false, Error = "User does not exist" });

                // Returns 0 if not authenticated
                long userId = await userService.LoginAsync(user);

                if (userId == 0)
                    return Unauthorized(new AuthResponseModel { Success = false });

                var token = authenticateService.GenerateToken(userId.ToString());

                return Ok(token);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [Authorize]
        [HttpPost("Refresh")]
        public IActionResult Refresh([FromHeader] string Authorization)
        {
            try
            {
                var refreshToken = Authorization.Replace("Bearer ", "");

                var token = authenticateService.Refresh(refreshToken);

                return Ok(token);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }
    }
}
