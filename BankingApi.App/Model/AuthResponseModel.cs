﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankingApi.App
{
    public class AuthResponseModel
    {
        public bool Success { get; set; }

        public string AccessToken { get; set; }

        public DateTime Expiry { get; set; }

        public string RefreshToken { get; set; }

        public string Error { get; set; }
    }
}
