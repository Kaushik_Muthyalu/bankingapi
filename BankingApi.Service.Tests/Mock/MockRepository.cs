﻿using BankingApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MockQueryable.FakeItEasy;

namespace BankingApi.Service.Tests
{
    public class MockRepository
    {
        public static IUserRepository UserRepository => new UserRepository(FakeDbContext.Context);

        public static IPasswordRepository PasswordRepository => new PasswordRepository(FakeDbContext.Context);

        public static ITransactionRepository TransactionRepository => new TransactionRepository(FakeDbContext.Context);
    }
}
