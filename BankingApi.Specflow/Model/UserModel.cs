﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankingApi.Specflow
{
    public class UserModel
    {
        public string Name { get; set; }

        public string Email { get; set; }


        public string DOB { get; set; }

        public decimal Balance { get; set; }
    }
}
