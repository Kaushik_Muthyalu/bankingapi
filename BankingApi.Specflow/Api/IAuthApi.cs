﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BankingApi.Specflow.Api
{
    interface IAuthApi
    {
        [Get("/api/Auth/Authenticate")]
        Task<ApiResponse<AuthResponseModel>> Authenticate(LoginModel login);
    }
}
