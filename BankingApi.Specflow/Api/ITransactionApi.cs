﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BankingApi.Specflow.Api
{
    interface ITransactionApi
    {
        [Get("/api/Transaction")]
        Task<List<TransactionModel>> Get();

        [Get("/api/Transaction/1")]
        Task<TransactionModel> GetSingle();

        [Post("/api/Deposit")]
        Task<TransactionModel> Deposit(TransactionModel transaction);

        [Post("/api/Deposit")]
        Task<TransactionModel> Withdraw(TransactionModel transaction);
    }
}
