﻿Feature: Auth

@mytag
Scenario: Login with valid credentials
	Given the email address is test@user.com
	And the password is test@123
	When the user is authenticated
	Then the user authentication should succeed
	And the response should be OK

Scenario: Login with incorrect password
	Given the email address is test@user.com
	And the password is incorrectpass
	When the user is authenticated
	Then the user authentication should fail
	And the response should be Unauthorized

Scenario: Login with non-existent user
	Given the email address is incorrect@email.com
	And the password is incorrectpass
	When the user is authenticated
	Then the user authentication should fail
	And the response should be NotFound