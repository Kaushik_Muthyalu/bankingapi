﻿using System;
using System.Collections.Generic;
using System.Text;
using BankingApi.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApplicationBuilderExtension
    {
        public static void DbInitialize(this IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetService<BankingContext>();

            dbContext.Database.EnsureCreated();
        }
    }
}
