﻿using System.Threading.Tasks;

namespace BankingApi.Service
{
    public interface IUserService
    {
        Task<bool> CheckUserExistsAsync(string email);
        Task<UserModel> GetUser(long id);
        Task<long> LoginAsync(LoginModel userModel);

        Task<long> TrySignUpAsync(SignUpModel userModel);
    }
}