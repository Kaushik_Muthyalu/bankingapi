﻿using BankingApi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionScope = System.Transactions.TransactionScope;

namespace BankingApi.Service
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository transactionRepository;
        private readonly IUserRepository userRepository;

        public TransactionService(ITransactionRepository transactionRepository, IUserRepository userRepository)
        {
            this.transactionRepository = transactionRepository;
            this.userRepository = userRepository;
        }

        public async Task<List<TransactionModel>> GetAllUserTransactionsAsync(long userId)
        {
            var dbTrasactions = await transactionRepository.GetTransactionsForUserAsync(userId);

            var transactions = (from transaction in dbTrasactions
                                select new TransactionModel
                                {
                                    Id = transaction.Id,
                                    Amount = Math.Abs(transaction.Amount),
                                    TransactionType = transaction.Amount > 0 ? "Credit" : "Debit",
                                    TransactionTime = transaction.TransactionTime.ToString("dd-MMM-yyyy hh:mm tt")
                                }).ToList();

            return transactions;
        }

        public async Task<TransactionModel> GetrTransactionAsync(long userId, long transactionId)
        {
            var dbTrasaction = await transactionRepository.GetTransactionAsync(userId, transactionId);

            var transaction = new TransactionModel
            {
                Id = dbTrasaction.Id,
                Amount = Math.Abs(dbTrasaction.Amount),
                TransactionType = dbTrasaction.Amount > 0 ? "Credit" : "Debit",
                TransactionTime = dbTrasaction.TransactionTime.ToString("dd-MMM-yyyy hh:mm tt")
            };

            return transaction;
        }

        public async Task<long> DepositAsync(long userId, TransactionModel transactionModel)
        {
            using (TransactionScope scope = HelperClass.GetTransactionScope())
            {
                decimal amount = transactionModel.Amount;

                var dbTransaction = await SaveTransactionAsync(userId, amount);

                decimal balance = await UpdateUserBalanceAsync(userId, amount);

                scope.Complete();

                return dbTransaction.Id;
            }
        }


        public async Task<long> WithdrawAsync(long userId, TransactionModel transactionModel)
        {
            using (TransactionScope scope = HelperClass.GetTransactionScope())
            {
                decimal amount = transactionModel.Amount * -1;

                var dbTransaction = await SaveTransactionAsync(userId, amount);

                decimal balance = await UpdateUserBalanceAsync(userId, amount);

                scope.Complete();

                return dbTransaction.Id;
            }
        }



        private async Task<Transaction> SaveTransactionAsync(long userId, decimal amount)
        {

            var transaction = new Transaction
            {
                UserId = userId,
                Amount = amount,
                TransactionTime = DateTime.UtcNow
            };

            await transactionRepository.SaveTransactionAsync(transaction);

            return transaction;
        }

        private async Task<decimal> UpdateUserBalanceAsync(long userId, decimal amount)
        {
            var user = await userRepository.GetUserAsync(userId);

            user.Balance += amount;

            await userRepository.UpdateUserAsync(user);

            return user.Balance;
        }
    }
}
