﻿using BankingApi.Repository;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using MockQueryable.FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace BankingApi.Service.Tests
{
    public static class FakeDbContext
    {
        private static BankingContext _context;

        public static BankingContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = A.Fake<BankingContext>(x => x.WithArgumentsForConstructor(new[] { new DbContextOptions<BankingContext>() }));

                    _context.Users = MockUsers;
                    _context.Passwords = MockPasswords;
                    _context.Transactions = MockTransactions;
                }

                return _context;
            }
        }

        private static DbSet<User> MockUsers =>
            new List<User>
            {
                new User {Id = 1, DateOfBirth = new DateTime(1973, 4, 9), Balance = 225.20M, Email = "test@work.com", Name = "Nobody" }
            }.AsQueryable()
            .BuildMockDbSet();

        private static DbSet<Password> MockPasswords =>
            new List<Password>
            {
                new Password {Id = 1, UserId = 1, HashedPassword = "TestPass", IsCurrent= true, SetTime = new DateTime(2021, 4, 30, 12, 04, 32)}
            }.AsQueryable().BuildMockDbSet();

        private static DbSet<Transaction> MockTransactions =>
            new List<Transaction>
            {
                new Transaction {Id = 1, UserId = 1, Amount = 625.20M, TransactionTime = new DateTime(2021, 4, 30, 14, 21, 03) },
                new Transaction {Id = 2, UserId = 1, Amount = -400, TransactionTime = new DateTime(2021, 5, 01, 11, 04, 02) }
            }.AsQueryable().BuildMockDbSet();

    }
}
