﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BankingApi.Repository
{
    public class Password
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey("User")]
        public long UserId { get; set; }

        [Required]
        public string HashedPassword { get; set; }

        public bool IsCurrent { get; set; }

        public DateTime SetTime { get; set; }
    }
}
