using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Net;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Net.Mail;

namespace BankingApi.App
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly ILogger<AuthenticateService> logger;
        private readonly JWTTokenConstants constants;

        public AuthenticateService(ILogger<AuthenticateService> logger, JWTTokenConstants constants)
        {
            this.logger = logger;
            this.constants = constants;
        }

        public AuthResponseModel GenerateToken(string sub)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, sub),
            };

            var secretBytes = Encoding.UTF8.GetBytes(constants.Secret);
            var key = new SymmetricSecurityKey(secretBytes);

            var signingCredentials = new SigningCredentials(key, constants.Algorithm);

            var token = new JwtSecurityToken(
                issuer: constants.Issuer,
                audience: constants.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow + constants.Lifetime,
                signingCredentials: signingCredentials);

            var refreshToken = new JwtSecurityToken(
                issuer: constants.Issuer,
                audience: constants.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                signingCredentials: signingCredentials);

            var tokenHandler = new JwtSecurityTokenHandler();

            return new AuthResponseModel
            {
                Success = true,
                AccessToken = tokenHandler.WriteToken(token),
                Expiry = token.ValidTo,
                RefreshToken = tokenHandler.WriteToken(refreshToken)
            };
        }

        public AuthResponseModel Refresh(string refreshTokenString)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var refreshtoken = tokenHandler.ReadJwtToken(refreshTokenString);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, refreshtoken.Subject),
            };

            var secretBytes = Encoding.UTF8.GetBytes(constants.Secret);
            var key = new SymmetricSecurityKey(secretBytes);

            var signingCredentials = new SigningCredentials(key, constants.Algorithm);

            var token = new JwtSecurityToken(
                issuer: constants.Issuer,
                audience: constants.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow + constants.Lifetime,
                signingCredentials: signingCredentials);

            return new AuthResponseModel
            {
                Success = true,
                AccessToken = tokenHandler.WriteToken(token),
                Expiry = token.ValidTo,
                RefreshToken = refreshTokenString
            };
        }
    }
}