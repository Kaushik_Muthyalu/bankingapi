﻿using BankingApi.Specflow.Api;
using FluentAssertions;
using Refit;
using System.Net;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace BankingApi.Specflow.Steps
{
    [Binding]
    public sealed class AuthStepDefinitions
    {

        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        private IAuthApi AuthApi => RestService.For<IAuthApi>("http://localhost:5000");

        private string email;
        private string password;

        ApiResponse<AuthResponseModel> Response;

        public AuthStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Given("the email address is (.*)")]
        public void GivenTheUserEmailIs(string email)
        {
            this.email = email;
        }

        [Given("the password is (.*)")]
        public void GivenThePasswordIs(string password)
        {
            this.password = password;
        }

        [When("the user is authenticated")]
        public async Task WhenTheUserIsAuthenticated()
        {
            Response = await AuthApi.Authenticate(new LoginModel
            {
                Email = email,
                Password = password
            });
        }

        [StepArgumentTransformation]
        public bool TransformResultVerbToBool(string result)
        {
            if (result == "succeed")
                return true;

            return false;
        }

        [Then(@"the user authentication should (.*)")]
        public void ThenTheUserAuthenticationIs(bool authenticated)
        {
            Response.Content.Should().NotBeNull();
            Response.Content.Success.Should().Be(authenticated);
        }

        [StepArgumentTransformation]
        public int TransformResponseToStatusCode(string result)
        {
            if (result.ToUpper() == "OK")
                return 200;
            if (result.ToUpper() == "NOTFOUND")
                return 404;
            if (result.ToUpper() == "UNAUTHORIZED")
                return 401;
            return 0;
        }

        [Then(@"the response should be (.*)")]
        public void ThenTheResponseCodeShouldBe(int result)
        {

            Response.StatusCode.Should().Be(result);
            Response.Content.Should().NotBeNull();
            Response.Content.Success.Should().BeTrue();
        }
    }
}
