﻿using BankingApi.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankingApi.App
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class TransactionController : ControllerBase
    {

        private readonly ILogger<TransactionController> _logger;
        private readonly ITransactionService transactionService;

        private readonly long _userId;

        public TransactionController(ILogger<TransactionController> logger, IAuthorizeService authorizeService, ITransactionService transactionService)
        {
            _logger = logger;
            this.transactionService = transactionService;

            _userId = Convert.ToInt64(authorizeService.Sub);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var transactions = await transactionService.GetAllUserTransactionsAsync(_userId);

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [HttpGet("/{transactionId}")]
        public async Task<IActionResult> Get(long transactionId)
        {
            try
            {
                var transactions = await transactionService.GetrTransactionAsync(_userId, transactionId);

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Deposit")]
        public async Task<IActionResult> Deposit([FromBody] TransactionModel transaction)
        {
            try
            {
                var transactionId = await transactionService.DepositAsync(_userId, transaction);

                if(transactionId==0)
                {
                    return new StatusCodeResult(500);
                }

                var createdTransaction = await transactionService.GetrTransactionAsync(_userId, transactionId);

                var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}/Transaction/{transactionId}";

                return Created(url, createdTransaction);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }

        [HttpPost("Withdraw")]
        public async Task<IActionResult> Withdraw([FromBody] TransactionModel transaction)
        {
            try
            {
                var transactionId = await transactionService.WithdrawAsync(_userId, transaction); if (transactionId == 0)
                {
                    return new StatusCodeResult(500);
                }

                var createdTransaction = await transactionService.GetrTransactionAsync(_userId, transactionId);

                var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}/Transaction/{transactionId}";

                return Created(url, createdTransaction);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");

                return new StatusCodeResult(500);
            }
        }
    }
}
