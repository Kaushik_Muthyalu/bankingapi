using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace BankingApi.App
{
    public class AuthorizeService : IAuthorizeService
    {
        public AuthorizeService(IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey(HeaderNames.Authorization))
            {
                string bearer = httpContextAccessor.HttpContext.Request.Headers[HeaderNames.Authorization];

                var token = new JwtSecurityToken(bearer.Replace("Bearer ", ""));

                Sub = token.Subject;
            }
        }

        public string Sub { get; private set; }
    }
}