﻿namespace BankingApi.App
{
    public interface IAuthenticateService
    {
        AuthResponseModel GenerateToken(string sub);
        AuthResponseModel Refresh(string refreshTokenString);
    }
}