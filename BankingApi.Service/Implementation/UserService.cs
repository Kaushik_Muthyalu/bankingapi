﻿using BankingApi.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BankingApi.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IPasswordRepository passwordRepository;

        public UserService(IUserRepository userRepository, IPasswordRepository passwordRepository)
        {
            this.userRepository = userRepository;
            this.passwordRepository = passwordRepository;
        }

        public async Task<bool> CheckUserExistsAsync(string email)
        {
            return await userRepository.CheckIfEmailExistsAsync(email);
        }

        public async Task<UserModel> GetUser(long id)
        {
            var dbUser = await userRepository.GetUserAsync(id);

            return new UserModel
            {
                Name = dbUser.Name,
                Email = dbUser.Email,
                DOB = dbUser.DateOfBirth.ToString("dd-MMM-yyyy"),
                Balance = dbUser.Balance
            };
        }

        public async Task<long> TrySignUpAsync(SignUpModel userModel)
        {
            // This will be hashed in an actual scenario
            string hashedPass = userModel.Password;

            using (TransactionScope scope = HelperClass.GetTransactionScope())
            {
                var user = new User
                {
                    Name = userModel.Name,
                    Email = userModel.Email,
                    DateOfBirth = Convert.ToDateTime(userModel.DOB),
                    Balance = 0
                };

                await userRepository.AddUserAsync(user);

                var password = new Password
                {
                    UserId = user.Id,
                    HashedPassword = hashedPass,
                    IsCurrent = true,
                    SetTime = DateTime.UtcNow
                };

                await passwordRepository.SavePasswordAsync(password);

                scope.Complete();

                return user.Id;
            }
        }

        /// <summary>
        /// Returns the user Id if authenticated, zero otherwise
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public async Task<long> LoginAsync(LoginModel userModel)
        {
            var user = await userRepository.GetUserByEmailAsync(userModel.Email);

            // This will be hashed in an actual scenario
            string hashedPass = userModel.Password;

            var password = await passwordRepository.GetCurrentForUserAsync(user.Id);

            if (password.HashedPassword != hashedPass)
                return 0;

            return user.Id;
        }
    }
}
