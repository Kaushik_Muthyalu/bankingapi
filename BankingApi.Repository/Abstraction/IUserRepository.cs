﻿using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public interface IUserRepository
    {
        Task<bool> CheckIfEmailExistsAsync(string email);
        Task<User> GetUserAsync(long id);
        Task<User> GetUserByEmailAsync(string email);
        Task AddUserAsync(User user);
        Task UpdateUserAsync(User user);
    }
}