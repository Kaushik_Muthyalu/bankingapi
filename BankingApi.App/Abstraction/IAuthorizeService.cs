﻿namespace BankingApi.App
{
    public interface IAuthorizeService
    {
        string Sub { get; }
    }
}