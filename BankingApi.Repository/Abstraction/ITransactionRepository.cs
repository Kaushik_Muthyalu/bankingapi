﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public interface ITransactionRepository
    {
        Task<Transaction> GetTransactionAsync(long userId, long transactionId);
        Task<List<Transaction>> GetTransactionsForUserAsync(long userId);
        Task SaveTransactionAsync(Transaction transaction);
    }
}