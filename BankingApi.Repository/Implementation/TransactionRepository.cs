﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly BankingContext context;

        public TransactionRepository(BankingContext context)
        {
            this.context = context;
        }

        public async Task<List<Transaction>> GetTransactionsForUserAsync(long userId)
        {
            return await context.Transactions.Where(o => o.UserId == userId).ToListAsync();
        }

        public async Task<Transaction> GetTransactionAsync(long userId, long transactionId)
        {
            return await context.Transactions.FirstOrDefaultAsync(o => o.UserId == userId && o.Id == transactionId);
        }

        public async Task SaveTransactionAsync(Transaction transaction)
        {
            await context.AddAsync(transaction);
            await context.SaveChangesAsync();
        }
    }
}
