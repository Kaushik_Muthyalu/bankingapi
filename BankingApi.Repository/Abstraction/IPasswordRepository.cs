﻿using System.Threading.Tasks;

namespace BankingApi.Repository
{
    public interface IPasswordRepository
    {
        Task<Password> GetCurrentForUserAsync(long userId);
        Task SavePasswordAsync(Password password);
    }
}