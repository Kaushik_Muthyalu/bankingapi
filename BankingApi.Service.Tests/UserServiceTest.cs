using FakeItEasy;
using System;
using Xunit;

namespace BankingApi.Service.Tests
{
    public class UserServiceTest : IDisposable
    {
        IUserService userService;

        public UserServiceTest()
        {
            userService = new UserService(MockRepository.UserRepository, MockRepository.PasswordRepository);
        }

        public void Dispose()
        {
            userService = null;
        }

        [Fact]
        public async void TrySignUpAsync_UserAlreadyExists_False()
        {
            // Arrange
            var signUp = new SignUpModel { Email = "test@work.com", DOB = "2021-04-05", Name = "Test", Password = "Password" };

            // Act
            long userId = await userService.TrySignUpAsync(signUp);

            // Assert
            Assert.Equal(0, userId);
        }

        [Fact]
        public async void TrySignUpAsync_NewUser_True()
        {
            // Arrange
            var signUp = new SignUpModel { Email = "user@email.com", DOB = "2021-04-05", Name = "Whatever", Password = "NoPass" };

            // Act
            long userId = await userService.TrySignUpAsync(signUp);

            // Assert
            Assert.NotEqual(0, userId);
        }

        [Theory]
        [InlineData("test@work.com", "TestPass", 1)]
        public async void LoginAsync_Authenticated_Number(string email, string password, long userId)
        {
            // Arrange
            var login = new LoginModel { Email = email, Password = password };

            // Act
            long result = await userService.LoginAsync(login);

            // Assert
            Assert.Equal(userId, result);
        }

        [Fact]
        public async void LoginAsync_IncorrectPassword_0()
        {
            // Arrange
            var login = new LoginModel { Email = "test@work.com", Password = "sfasdf" };

            // Act
            long result = await userService.LoginAsync(login);

            // Assert
            Assert.Equal(0, result);
        }

        [Theory]
        [InlineData("example@notfound.com", "ouwrej;l")]
        public async void LoginAsync_UserNotFound_0(string email, string password)
        {
            // Arrange
            var login = new LoginModel { Email = email, Password = password };

            // Act
            long result = await userService.LoginAsync(login);

            // Assert
            Assert.Equal(0, result);
        }
    }
}
