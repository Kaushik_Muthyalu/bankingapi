﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using BankingApi.Repository;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtension
    {
        public static void AddRepository(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<BankingContext>(builder =>
            {
                //builder.UseSqlServer(Configuration.GetConnectionString("BankingDatabase"));
                builder.UseSqlServer(connectionString);
                //builder.UseSqlite(connectionString);
            });

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPasswordRepository, PasswordRepository>();
            services.AddTransient<ITransactionRepository, TransactionRepository>();
        }
    }
}
