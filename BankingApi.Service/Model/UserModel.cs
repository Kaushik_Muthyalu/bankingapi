﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankingApi.Service
{
    public class UserModel
    {
        public string Name { get; set; }

        public string Email { get; set; }


        public string DOB { get; set; }

        public decimal Balance { get; set; }
    }
}
