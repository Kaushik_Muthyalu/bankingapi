﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;

namespace BankingApi.App
{
    public class JWTTokenConstants
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public string Secret { get; set; }

        public string RefreshSecret { get; set; }

        public TimeSpan Lifetime { get; set; }

        public string Algorithm { get; set; }
    }
}