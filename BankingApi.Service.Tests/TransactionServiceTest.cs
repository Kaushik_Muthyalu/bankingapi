﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankingApi.Service.Tests
{
    public class TransactionServiceTest
    {
        ITransactionService transactionService;

        public TransactionServiceTest()
        {
            transactionService = new TransactionService(MockRepository.TransactionRepository, MockRepository.UserRepository);
        }

        [Theory]
        [InlineData(1, 1000)]
        public async void DepositAsync_AmountDeposited_NotZero(long userId, decimal depositAmount)
        {
            // Arrange
            var transaction = new TransactionModel { Amount = depositAmount };

            // Act
            var transactionId = await transactionService.DepositAsync(userId, transaction);

            // Assert
            Assert.Equal(0, transactionId);
        }

        [Theory]
        [InlineData(1, 200)]
        public async void WithdrawAsync_AmountWithdrew_NotZero(long userId, decimal withdrawAmount)
        {
            // Arrange
            var transaction = new TransactionModel { Amount = withdrawAmount };

            // Act
            var transactionId = await transactionService.WithdrawAsync(userId, transaction);

            // Assert
            Assert.NotEqual(0, transactionId);
        }
    }
}
